package com.spaneos.scb.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spaneos.scb.domain.Contact;
import com.spaneos.scb.repo.ContactRepo;
import com.spaneos.scb.repo.Utility;

@Service
public class ContactServiceImpl implements ContactService {
	@Autowired
	private ContactRepo contactRepo;
	
	private static final Logger LOGGER=LoggerFactory.getLogger(ContactServiceImpl.class);

	@Override
	public Contact addContact(Contact contact) {
		return contactRepo.save(contact);
	}

	@Override
	public Contact updateContact(Contact contact) {
		return contactRepo.save(contact);
	}

	@Override
	public String removeContact(int cid) {
		Contact contact = contactRepo.getOne(cid);
		if (contact != null) {
			contactRepo.delete(contact);
			return "Contact with id " + contact.getCid() + " is deleted successfully";
		}
		return "Contact is not found with given id " + cid;
	}

	@Override
	public List<Contact> getContacts() {
		return contactRepo.findAll();
	}

	@Override
	public List<Contact> searchContact(String searchStr) {
		List<Contact> list = null;
		LOGGER.info("Search string : {}",searchStr);
		Iterable<Contact> iterator = contactRepo.findAll(Utility.nameMatched(searchStr));
		if (iterator != null) {
			list = new ArrayList<>();
			iterator.forEach(list::add);
			LOGGER.info("Total : {} recods found for search criteria : {}",list.size(),searchStr);
		}
		return list;
	}

}
