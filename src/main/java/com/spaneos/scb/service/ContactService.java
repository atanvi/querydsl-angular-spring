package com.spaneos.scb.service;

import java.util.List;

import com.spaneos.scb.domain.Contact;

public interface ContactService {

	Contact addContact(Contact contact);

	Contact updateContact(Contact contact);

	String removeContact(int cid);

	List<Contact> getContacts();

	List<Contact> searchContact(String searchStr);

}
