package com.spaneos.scb.web;

import java.util.List;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spaneos.scb.domain.Contact;
import com.spaneos.scb.service.ContactService;

@RestController
@RequestMapping("api/")
public class ContactController {
		
		private static final Logger LOGGER=LoggerFactory.getLogger(ContactController.class);
		
		@Autowired
		private ContactService contactService;
	
	
		@GetMapping("index")
		public String greetMessage(){
			return "Welcome to Contact app";
		}
		
		@PostMapping("contact/")
		public Contact addContact(@RequestBody Contact contact){			
			Contact savedContact=contactService.addContact(contact);
			return savedContact;
			
		}
		
		@PutMapping("contact/")
		public Contact updateContact(@RequestBody Contact contact){
			Contact updatedContact=contactService.updateContact(contact);
			return updatedContact;
		}
		
		@DeleteMapping("contact/{cid}")
		public String removeContact(@PathParam("cid") int cid){
			String message = contactService.removeContact(cid);
			return message;
		}
		
		@GetMapping("contact/all")
		public List<Contact> viewContacts(){
			List<Contact> contactList=contactService.getContacts();
			return contactList;
		}
		
		@GetMapping("contact/search/{searchStr}")
		public List<Contact> searchContact(@PathVariable("searchStr") String searchStr){
			LOGGER.info("Search string path param value : {}",searchStr);
			List<Contact> contactList=contactService.searchContact(searchStr);
			return contactList;
		}
		
}
