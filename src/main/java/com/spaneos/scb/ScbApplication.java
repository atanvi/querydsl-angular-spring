package com.spaneos.scb;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.spaneos.scb.domain.Contact;
import com.spaneos.scb.repo.ContactRepo;

@SpringBootApplication
public class ScbApplication implements CommandLineRunner {
	@Autowired
	private ContactRepo contactRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(ScbApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
			contactRepo.deleteAll();
			
			Contact contact1=new Contact("LAKSHMAN","A","lakshman.a@spaneos.com","9036102111");
			Contact contact2=new Contact("PRADEEP","KM","pradeep.km@spaneos.com","9036102112");
			Contact contact3=new Contact("LAXMI","AK","laxmi.ak@spaneos.com","9036102113");
			Contact contact4=new Contact("MAHESH","CS","mahesh.cs@spaneos.com","9036102114");
			Contact contact5=new Contact("VENKATESH","N","venkatesh.n@spaneos.com","9036102115");
			Contact contact6=new Contact("VENKATESH","CS","venkatesh.cs@spaneos.com","9036102116");
			Contact contact7=new Contact("VENKATESH","P","venkatesh.p@spaneos.com","9036102117");
			Contact contact8=new Contact("KIRAN","U","kiran.u@spaneos.com","9036102118");
			Contact contact9=new Contact("BASANTH","G","basanth.g@spaneos.com","9036102119");
			Contact contact10=new Contact("MADHUKAR","AG","madhukar.g@spaneos.com","9036102110");
			
			List<Contact> contactList = Arrays.asList(contact1,contact2,contact3,contact4,contact5,contact6,contact7,contact8,contact9,contact10);
			contactRepo.save(contactList);
			
		
				
		
	}
	
	
}
