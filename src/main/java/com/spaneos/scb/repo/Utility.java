package com.spaneos.scb.repo;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.spaneos.scb.domain.QContact;

public class Utility {
		
	public static BooleanExpression nameMatched(String str){
		QContact contact=QContact.contact;
		return contact.firstName.contains(str).or(contact.lastName.contains(str)).or(contact.email.contains(str)).or(contact.mobile.contains(str));
	}
}
