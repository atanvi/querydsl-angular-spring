package com.spaneos.scb.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.spaneos.scb.domain.Contact;


public interface ContactRepo extends JpaRepository<Contact, Integer>,QueryDslPredicateExecutor<Contact>{ 
	

	
}