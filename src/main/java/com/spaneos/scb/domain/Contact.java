package com.spaneos.scb.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Contact {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int cid;
		private String firstName;
		private String lastName;
		private String mobile;
		private String email;
			
		public Contact() {
		
		}
		public Contact(String firstName, String lastName, String mobile, String email) {
		
			this.firstName = firstName;
			this.lastName = lastName;
			this.mobile = mobile;
			this.email = email;
		}
		public int getCid() {
			return cid;
		}
		public void setCid(int cid) {
			this.cid = cid;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getMobile() {
			return mobile;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		
}
