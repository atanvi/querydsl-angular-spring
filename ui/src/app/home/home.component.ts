import { AppService } from './../app.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Employee } from '../employee/employee';
import { FormBuilder, FormGroup,FormControl} from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  
  contacts:Employee[];
  seachControl = new FormControl();
  
  constructor(private appService:AppService) { }
 
  ngOnInit() {
    this.seachControl.valueChanges.subscribe(value => {
         this.search(value);
    });
  }
  search(searchStr){
      console.log("Search string :",searchStr);
      this.contacts=this.appService.search(searchStr);
      console.log(this.contacts);
  }
}
