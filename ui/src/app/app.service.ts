import { Employee } from './employee/employee';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {

  constructor(private http:HttpClient){
  }

  search(searchStr){
      if(searchStr.trim().length!=0){
       let employees:Employee[];
       this.http.get<Employee[]>("/api/contact/search/"+searchStr).subscribe(response=>{
            employees=response;
            console.log(employees);
      },error=>{
          console.log(error);
      })
      return employees;
    }
  }

}
